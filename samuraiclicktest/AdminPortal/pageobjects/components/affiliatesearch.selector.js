import { Selector, t } from 'testcafe';

export default class AffiliateSearchSelector {
    constructor(){
        this.menulabel = Selector('#affiliatesmenu');
        this.affiliateid = Selector('.form-group > #affiliateId');
        this.searchbutton = Selector('#submitSearch');
        this.tablefirstcolumn = Selector('[data-uid="5fefce41-a7ed-40d9-aab7-86f56992428d"] > :nth-child(1)');
        this.tablesecondcolumn = Selector('[data-uid="5fefce41-a7ed-40d9-aab7-86f56992428d"] > :nth-child(2)');
        this.tablethirdcolumnfirstrow = Selector(':nth-child(1) > .medium-id');
        this.tablefourthcolumn = Selector(':nth-child(1) > .medium-name');
        this.tablefifthcolumn = Selector(':nth-child(1) > .medium-url');
        this.tablesixthcolumn = Selector('[data-uid="5fefce41-a7ed-40d9-aab7-86f56992428d"] > :nth-child(4)');
        this.tablesevencolumn = Selector('[data-uid="5fefce41-a7ed-40d9-aab7-86f56992428d"] > :nth-child(5) > .k-icon');
        this.getreportbutton = Selector('#link_button_117');

    }


}