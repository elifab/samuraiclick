import { Selector, t } from 'testcafe';
import Credentials from '../../config/config.adminportal';

const loginCredentials = new Credentials();

export default class LoginSelector {
    constructor () {
        this.username = Selector('#inputName');
        this.password = Selector('#inputPassword');
        this.button = Selector('.btn');
    }
}

