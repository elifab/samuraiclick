import { Selector } from 'testcafe'; 
import Credentials from '../../config/config.adminportal';
import Login from '../../stepdefinition/user-login';

const loginCredentials = new Credentials();
const login = new Login()
const url = `http://dev-admin.samuraiclick.com/`;

fixture('Login Test').page(url);  //intiate test

test('Verify that user can access the Admin Portal using a valid admin credentials', async t => {
    login.userlogin(loginCredentials.useradmin, loginCredentials.password, t)
});

test('Verify that user can access the Admin Portal using a valid operations credentials', async t => {
    login.userlogin(loginCredentials.useroperations, loginCredentials.password, t)
});