import { Selector } from 'testcafe'; 
import Login from '../../stepdefinition/user-login';
import Credentials from '../../config/config.adminportal';
import AffiliateSearchSelector from '../components/affiliatesearch.selector';
import AffiliateSearch from '../../stepdefinition/affiliate-search';


const login = new Login();
const loginCredentials = new Credentials();
const affiliateSearchSelector = new AffiliateSearchSelector();
const affiliatesearch = new AffiliateSearch();
const url = `http://dev-admin.samuraiclick.com/`;

fixture('Affiliate Search')
    .page(url)
    .beforeEach(async t=> {
        login.userlogin(loginCredentials.useradmin, loginCredentials.password, t)
        
        await t
            .click(affiliateSearchSelector.menulabel)
    });
    
test('Verify that Affiliate menu is available in the Admin Portal', async t => {
    affiliatesearch.checklabel(t);
});

test('Verify that Affiliate Management page should be accessible by admin', async t =>{
    affiliatesearch.access(t);
});

test('Verify that searching an affiliate using an Affiliate ID only is allowed', async t=>{ 
    affiliatesearch.search(affiliateSearchSelector.affiliateid, '117', t);
});

test('Verify that a result will be displayed if an affiliate record is searched using an existing Affiliate ID', async t=>{
    affiliatesearch.verify(t);

});
    