import { Selector } from 'testcafe'; 
import AffiliateSearchSelector from '../pageobjects/components/affiliatesearch.selector';


const affiliateSearchSelector = new AffiliateSearchSelector();


export default class AffiliateSearch{
    async checklabel (t){
        await t
             .expect((affiliateSearchSelector.menulabel).innerText).contains('Affiliates'); 
    }

    async access (t){
        await t
             .click(affiliateSearchSelector.menulabel)       
    }

    async search (selector, value, t){
        await t
             .typeText(selector, value)
             .click(affiliateSearchSelector.searchbutton)     
    }
    async verify (t){
        await t
             .expect((affiliateSearchSelector.tablefirstcolumn).innerText).eql('55');
    }
}



