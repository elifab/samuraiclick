import { Selector } from 'testcafe'; 
import LoginSelector from '../pageobjects/components/login.selector';

const loginSelector = new LoginSelector();

export default class Login{
    async userlogin (username, password, t){
        await t 
            .typeText(loginSelector.username, username)
            .typeText(loginSelector.password, password)
            .click(loginSelector.button)
    }
}

 